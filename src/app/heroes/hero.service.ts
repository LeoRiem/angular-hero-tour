import { Injectable } from '@angular/core';

import { Hero }   from '../models/hero';
import { HEROES } from '../models/mock-heroes';

import { Observable } from 'rxjs/Observable';
import { of }         from 'rxjs/observable/of';

import { MessageService } from '../messages/message.service';

@Injectable()
export class HeroService {

constructor(private messageService: MessageService) { }

  getHeroes(): Observable<Hero[]> {
    this.messageService.add('Heroes fetched !!!');
    return of(HEROES);
  }

  getHero(id: number): Observable<Hero> {
    // TODO: send the message _after_ fetching the hero
    this.messageService.add(`HeroService: fetched hero with id : ${id}`);
    return of(HEROES.find(hero => hero.id === id));
  }
}
